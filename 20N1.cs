using System;
using System.Linq;

class Student
{
    public string FN;
    public string SN;
    public string MN;
    public int BD;
    public string BM;
    public int BY;
    public int GN;
    public string AP;

}

class Program
{

    static void Main(string[] args)
    {
        
        Student[] students = new Student[3];
        Console.WriteLine("Введите данные первого студента:");
        for (int i = 0; i < 3; i++)
        {
            Console.WriteLine("Введите данные " + (i + 1) + "-го студента:");
            students[i] = new Student();
            Console.Write("Имя:");
            students[i].FN = Console.ReadLine();

            Console.Write("Фамилия:");
            students[i].SN = Console.ReadLine();

            Console.Write("Отчество:");
            students[i].MN = Console.ReadLine();

            Console.Write("День рождения (числом):");

            try
            {
                 students[i].BD = Convert.ToInt32(Console.ReadLine());
                
            }
            catch (Exception )
            {
                Console.WriteLine("Вы ввели не число!");
                Console.Write("Попроуйте заново:");

                students[i].BD = Convert.ToInt32(Console.ReadLine());
            }

            Console.Write("Месяц рождения (словом):");
            students[i].BM = Console.ReadLine();

            Console.Write("Год рождения (числом):");

            try
            {
                students[i].BY = Convert.ToInt32(Console.ReadLine());

            }
            catch (Exception)
            {
                Console.WriteLine("Вы ввели не число");
                Console.Write("Попроуйте заново:");

                students[i].BY = Convert.ToInt32(Console.ReadLine());
            }

            Console.Write("Номер группы:");

            try
            {
                students[i].GN = Convert.ToInt32(Console.ReadLine());

            }
            catch (Exception)
            {
                Console.WriteLine("Вы ввели не число");
                Console.Write("Попроуйте заново:");

                students[i].GN = Convert.ToInt32(Console.ReadLine());
            }

            Console.Write("Успеваемость (удовлетворительно/хорошо/отлично):");
            students[i].AP = Console.ReadLine();
        }


        Console.WriteLine(" ");

        ///От сюда уже пошел вывод 


        for (int i = 0; i < 3; i++)
        {
            Console.WriteLine("Студент номер " + (i + 1) + ":");

            Console.Write("ФИО:");
            Console.WriteLine(students[i].SN + " " + students[i].FN + " " + students[i].MN);
            Console.Write("Дата рождения:");
            Console.WriteLine(students[i].BD + " " + students[i].BM + " " + students[i].BY);
            Console.Write("Номер группы:");
            Console.WriteLine(students[i].GN);
            Console.Write("Успеваемость:");
            Console.WriteLine(students[i].AP);
        }



        Console.Read();
    }


}