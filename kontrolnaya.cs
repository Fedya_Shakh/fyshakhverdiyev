using System;

class Program
{
    static void Main(string[] args)
    {
        Console.Write("Введите размер квадратной матрицы: ");
        int n = Convert.ToInt32(Console.ReadLine());
        if (n <= 0)
        {
            Console.WriteLine("Неверный размер!");
            return;
        }
        int[,] mass = new int[n, n];
        int[] arr1 = new int[n];
        for (int i = 0; i < n; i++)
        {
            for (int j = 0; j < n; j++)
            {
                Console.Write("Введите элемент №" + (i * n + j + 1) + " :");
                mass[i, j] = Convert.ToInt32(Console.ReadLine());
            }
        }
        Console.WriteLine("Массив:");
        for (int i = 0; i < n; i++)
        {
            for (int j = 0; j < n; j++)
            {
                Console.Write("\t" + mass[i, j]);

            }
            Console.WriteLine();
        }
        for (int i = 0; i < n; i++)
        {
            for (int j = 0; j < n - i; j++)
            {
                arr1[i] += mass[j , j + i];
            }
            for (int j = n - 1; j > n - i - 1; j--)
            {
                arr1[i] += mass[j , j - n + i];
            }
        }
        int max = 0;
        for (int i = 0; i < n; i++)
        {
            if (arr1[i] > max)
            {
                max = arr1[i];
            }
        }
        Console.WriteLine(max);

        Console.ReadLine();
    }
}
