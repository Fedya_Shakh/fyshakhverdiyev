using System;

namespace ConsoleApp1
{
    public class MyLinkedList2
    {
        private class Node
        {
            public int value;
            public Node prev;
            public Node next;

            public Node(int value)
            {
                this.value = value;
            }
        }

        private Node start;
        private Node end;
        private int count = 0;

        public MyLinkedList2(int[] arr)
        {
            foreach (var val in arr)
            {
                insert(val);
            }
        }

        public void insert(int k)
        {
            Node newNode = new Node(k);
            if (start == null)
            {
                start = newNode;
                end = newNode;
            }
            else if (start.value >= k)
            {
                newNode.next = start;
                start.prev = newNode;
                start = newNode;
            }
            else if (end.value <= k)
            {
                newNode.prev = end;
                end.next = newNode;
                end = newNode;
            }
            else
            {
                Node index = start;
                while (index != null)
                {
                    if (index.value >= k)
                    {
                        newNode.next = index;
                        newNode.prev = index.prev;
                        index.prev.next = newNode;
                        index.prev = newNode;
                        break;
                    }
                    index = index.next;
                }
            }
            count++;
        }

        public void delete(int k)
        {
            Node index = start;
            while (index != null)
            {
                if (index.value == k)
                {
                    if (index.prev != null)
                    {
                        index.prev.next = index.next;
                    }
                    if (index.next != null)
                    {
                        index.next.prev = index.prev;
                    }

                    if (index == start)
                    {
                        start = index.next;
                    }
                    if (index == end)
                    {
                        end = index.prev;
                    }
                    count--;
                    break;
                }
                index = index.next;
            }
        }

        public MyLinkedList2[] divide()
        {
            MyLinkedList2[] res = new MyLinkedList2[2];
            Node index = start;
            while (index != null)
            {
                if (index.value % 3 == 0)
                {
                    if (res[0] == null)
                    {
                        res[0] = new MyLinkedList2(new[] { index.value });
                    }
                    else
                    {
                        res[0].insert(index.value);
                    }
                }
                else
                {
                    if (res[1] == null)
                    {
                        res[1] = new MyLinkedList2(new[] { index.value });
                    }
                    else
                    {
                        res[1].insert(index.value);
                    }
                }
                index = index.next;
            }
            return res;
        }

        public MyLinkedList2 merge(MyLinkedList2 ll1)
        {
            Node index = ll1.start;
            MyLinkedList2 res = this;
            while (index != null)
            {
                insert(index.value);
                index = index.next;
            }

            return res;
        }

        public MyLinkedList2 newList()
        {
            MyLinkedList2 res = null;
            Node index = start;
            Node revIndex = end;
            for (int i = 0; i < count / 2 + count % 2; i++)
            {
                if (res == null)
                {
                    res = new MyLinkedList2(new[] { index.value * revIndex.value });
                }
                else
                {
                    res.insert(index.value * revIndex.value);
                }

                index = index.next;
                revIndex = revIndex.prev;
            }

            return res;
        }

        public override string ToString()
        {
            string res = "";
            Node index = start;
            while (index != null)
            {
                res += index.value + ",";
                index = index.next;
            }

            return res;
        }
    }
}
