﻿using System;

namespace Fibonachi
{
    class MyMathClass
    {
        static void Main()
        {
            Console.Write("Введите число факториал которого хотите полуичть:");
            int a = Convert.ToInt32(Console.ReadLine()); 
            
            Console.WriteLine("Ответ равен=" + Factorial(a));
        }

        static int Factorial(int a)
        {
            if (a == 0 || a == 1)
                return 1;

            return a * Factorial(a - 1);
        }
    }
}
