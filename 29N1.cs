﻿using System;

namespace smthing
{
    class Program
    {
        static void Main(string[] args)
        {
            int x = Convert.ToInt32(Console.ReadLine());
            int y = 0;
            if (x > 10)
            {
                while (x > 0)
                {
                    y = y * 10 + x % 10;
                    x /= 10;
                }
                Console.WriteLine(y);
            }
            else
            {
                Console.WriteLine("Введите число больше 10!");
            }

            Console.ReadLine();
        }
    }
}
