using System;
using System.Linq;

class Circ
{
    public int Rad;
    public int Diam;
    public int Leng;
    public double Sq;
    

}

class Program
{

    static void Main(string[] args)
    {
        
        Circ[] circs = new Circ[3];
        Console.WriteLine("Введите данные окружностей:");
        for (int i = 0; i < 3; i++)
        {
            Console.WriteLine("Введите данные окружности номер " + (i + 1) );
            circs[i] = new Circ();
            Console.Write("Радиус:");
            circs[i].Rad = Convert.ToInt32(Console.ReadLine());

            //Console.Write("Диаметр:");
            //circs[i].Diam = Convert.ToInt32(Console.ReadLine());
            circs[i].Diam = circs[i].Rad * 2 ;

            circs[i].Leng = circs[i].Diam;

            circs[i].Sq = Math.Pow(circs[i].Rad, 2);

        }


        Console.WriteLine(" ");

        ///От сюда уже пошел вывод 
        
        for (int i = 0; i < 3; i++)
        {
            Console.WriteLine("Окружность номер " + (i + 1) + ":");


            Console.Write("Радиус:");
            Console.WriteLine(circs[i].Rad);
            Console.Write(" ");
            Console.Write("Диаметр:");
            Console.WriteLine(circs[i].Diam);
            Console.Write(" ");
            Console.Write("Длинна:");
            Console.WriteLine(circs[i].Leng + "п");
            Console.Write(" ");
            Console.Write("Площадь:");
            Console.WriteLine(circs[i].Sq + "п");

        }



        Console.Read();
    }


}