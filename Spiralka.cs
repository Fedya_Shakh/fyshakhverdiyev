﻿using System;


namespace ClassWork
{
    
     class Spiral
    {
        static void Main()
        {
            Console.Write("Введите ширину массива:");
            int x = Convert.ToInt32(Console.ReadLine());

            Console.Write("Введите длинну массива:");
            int y = Convert.ToInt32(Console.ReadLine());
            if (x <= 6 && y <= 6)
            {

                var arr = new int[x, y];

                Console.WriteLine("Вводите элементы массива:");


                for (int i = 0; i < arr.GetLength(0); i++)
                {

                    for (int j = 0; j < arr.GetLength(1); j++)
                    {

                        arr[i, j] = Convert.ToInt32(Console.ReadLine());


                    }

                }

                var indent = 0;
                var row = arr.GetLength(0);
                var col = arr.GetLength(1);
                var stepCount = row / 2 + row % 2;
                for (int i = 1; i <= stepCount; i++)
                {

                    for (int j = indent; j < col - indent; j++)
                        Console.Write(arr[indent, j] + " ");


                    if (row > indent + indent + 2)
                    {
                        for (var j = indent + 1; j < row - indent - 1; j++)
                            Console.Write(arr[j, col - 1 - indent] + " ");
                    }

                    if (row > indent + indent + 1)
                    {
                        for (var j = col - indent - 1; j >= indent; j--)
                            Console.Write(arr[row - 1 - indent, j] + " ");
                    }


                    if (row > indent + indent + 2)
                    {
                        for (var j = row - indent - 2; j > indent; j--)
                            Console.Write(arr[j, indent] + " ");
                    }
                    indent++;
                }
            }
            else
            {
                Console.WriteLine("Ширина и высота долджны быть меньше или равны 6! ");
            }

            Console.ReadLine();
        }

            
        }
}

