﻿using System;

namespace Fibonachi
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("До какого числа считать ряд Фибоначчи?");
            int number = Convert.ToInt32(Console.ReadLine());

            int x = 1;
            Console.Write("{0} ", x);
            int y = 1;
            Console.Write("{0} ", y);
            int sum = 0;

            while (number >= sum)
            {
                sum = x + y;

                Console.Write("{0} ", sum);

                x = y;
                y = sum;
            }
        }
    }
}
