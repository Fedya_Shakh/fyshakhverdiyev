﻿using System;
using System.Linq;

class Program
{
    static void Main(string[] args)
    {
        Console.WriteLine("Мы создаем массив с определённой вместимостью.");
        Console.WriteLine("Заполняем его случайными числами от 0 до 10. ");
        Console.WriteLine("Проверим вашу удачу!");
        Console.WriteLine("");

        Console.Write("Введите кол-во элементов массива:");

        int count;

        count = Convert.ToInt32(Console.ReadLine());

        int[] mass = new int[count];
        //Для удобства установим ограничение в размерности матрицы(10)
        
        //if (count <= 10)
        {
            for (int i = 0; i < count; i++)
            {
                mass[i] = i;
            }

            Dich(mass);

            for (int i = 0; i < count; i++)
            {
                Console.Write(mass[i] + ";");

            }
            Console.WriteLine("");
        }
        

        //else
        //{
        //    Console.WriteLine("Кол-во элементов не может превышать 10!");

        //    Console.WriteLine("Удачи!");
        //}

        
    }

    private static void Dich(int[] mass)
    {
        int b = 0;
        Console.Write("Введите ваше число: ");
        int x = Convert.ToInt32(Console.ReadLine());
        int a = mass.Length;
        while (b < a)
        {
            int middle  = (b + a) / 2;
            if (mass[middle] == x)
            {
                Console.WriteLine("Ваша цифра есть в массиве: " + x);
                break;
            }
            else if (mass[middle] < x)
            {
                b = middle + 1;
            }
            else if (mass[middle] > x)
            {
                a = middle;
            }
            if (b == a)
                Console.WriteLine("Вашей цифры нет");
        }
    }
}

   
