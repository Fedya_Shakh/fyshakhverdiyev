namespace Solution
{
    public class Car
    {
        ///Поля:
        
        private string brand;
        public int Speed;
        private string color;
        public string direction;

        ///Свойства:
        public string Brand
        {
            get
            {
                return brand;
            }
            set 
            {
                brand = Console.ReadLine();
            }
        }

        public string Color
        {
            get
            {
                return color;
            }
            set
            {
                color = Console.ReadLine();
            }
        }

        ///Конструктор вот:

        public Car(int speed)
        {
            Speed = speed;
        }

        ///Методы: 
        public string RideForward() 
        {

            return direction = "Forward";

        }
        public string RideBackward()
        {

            return direction = "Backward";

        }
    }

    public class Racer
    {
        public int Driving_skill;
        public string Race;
        public string City;
        public string Name;

        public Racer(int driving_skill, string name, string race, string city)
        {
            Driving_skill = driving_skill;
            Race = race;
            City = city;
            Name = name;
        }
        public void Racer_and_Car(Car car, Racer racer)
        {
            Console.WriteLine($"{racer.Name} ездит на {car.Brand}");
        }
    }

}